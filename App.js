import { StatusBar } from "expo-status-bar";
import {TextInput, StyleSheet, Text, View, ScrollView, Button} from "react-native";
import {useState, useEffect} from "react"
import Layout from "./layoutDeTelaEstrutura";
import Layouthorizontal from "./layoutHorizontal";

export default function Login() {
  return (
    <View style={styles.container}>
      <Text>Login</Text>
      <TextInput style={styles.input} placeholder="Nome de Usuario"/>
      <TextInput style={styles.input} placeholder="Senha"/>
      <Button title="login" style={styles.button}></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(144, 233,10)'
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  input: {
    borderWidth:1,
    borderColor: 'gray',
    width: '80%',
    padding: 10,
    padding:10,
    marginVertical: 10
  },
});
