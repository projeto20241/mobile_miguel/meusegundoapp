import React, { useState } from 'react';
import { Button, Text, View, Image } from 'react-native';
import noia from './assets/noia.jpg';
import triste from './assets/triste.jpg'
import LayoutGrade from './layoutGrade';

const Cat = (props) => {
  const [isHungry, setIsHungry] = useState(true);

  function miguel() {
    setIsHungry(false); // Altera isHungry para false, indicando que o gato está alimentado
  }

  return (
    <View>
      <Text>
        I am {props.name}, and I am {isHungry ? 'hungry' : 'full'}!
      </Text>
      <Image source={isHungry ? triste : noia} style={{ width: 200, height: 200 }} /> 
      <Button
        onPress={miguel} 
        disabled={!isHungry}
        title={isHungry ? 'Pour me some milk, please!' : 'Thank you!'}
      />
    </View>
  );
};

const Cafe = () => {
  return (
    <>
      <Cat name="Munkustrap" />
    </>
  );
};

export default Cafe;
